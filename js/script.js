'use strict';
let accordionList = document.querySelectorAll('.accordion');

for (let elem of accordionList){
    elem.addEventListener('click', function(){
        this.nextElementSibling.classList.toggle('active-accordion');
        
    })
}

let sliderContainer = document.querySelector('.slider-container');
let sliderList = sliderContainer.querySelectorAll('.test');
let next = document.querySelector('.action-next');
let prev = document.querySelector('.action-prev');